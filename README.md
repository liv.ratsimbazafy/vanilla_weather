## VANILLA WEATHER
A basic vanilla JS based application for getting realtime weather data according to your location.

## Installation
    - Clone the repository

## Usage
    - Run index.html

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## version
MVP

## License
[MIT](https://choosealicense.com/licenses/mit/)